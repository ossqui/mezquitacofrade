import { ConectarService } from './../servicios/conectar.service';
import { AuthService } from './../servicios/auth.service';
import { ModalTemploPage } from './../modals/modal-templo/modal-templo.page';
import { Component, ViewChild } from '@angular/core';
import { TemploservicioService } from '../servicios/temploservicio.service';
import { LoadingController, ModalController, IonInfiniteScroll } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  @ViewChild('dynamicList') dynamicList;

  listado = [];
  listadoPanel = [];
  contadorImagenes = 0;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(private todoS: TemploservicioService,
    public loadingController: LoadingController,
    private ModalController: ModalController,
    private AuthService: AuthService,
    private comunicacion: ConectarService,
    private translate: TranslateService
  ) {
    this.presentLoading(this.translate.instant("loading"));
    this.comunicacion.getMessage().subscribe((dato) => {
      if (dato == true) {
        this.refrescar();
      }

    });
  }



  /**
   * Detiene el spinner de cargando cuando las imagenes estan completamente cargadas
   * @param e Envento opcional que recibe por parametro (no le he dado uso)
   */
  imagenCargada(e?) {

    if (++this.contadorImagenes == 2) {
      this.loadingController.dismiss();
    }

  }

  /**
   * Comprueba el estado de la autenticación del usuario
   */
  autenticado() {
    return this.AuthService.estadoAuth();
  }

  /**
   * Inicializa listadoPanel con los templos de la base de datos al entrar por primera vez
   */
  ionViewDidEnter() {

    this.todoS.leerTemplos()
      .subscribe((querySnapshot) => {
        this.listado = [];
        this.delete();
        querySnapshot.forEach((doc) => {
          this.listado.push({ id: doc.id, ...doc.data() });
        });
        this.listadoPanel = this.listado;
        this.infiniteScroll.disabled = false;
      });
  }

  /**
   * Recarga la lista de templos de la base de datos al ejecutarse
   */
  refrescar() {
    this.presentLoading(this.translate.instant("loading"));
    this.todoS.leerTemplos()
      .subscribe((querySnapshot) => {
        this.listado = [];
        this.delete();
        querySnapshot.forEach((doc) => {
          // doc.data() is never undefined for query doc snapshots
          //console.log(doc.id, " => ", doc.data());
          this.listado.push({ id: doc.id, ...doc.data() });
        });
        //console.log(this.listado);
        this.listadoPanel = this.listado;
        this.loadingController.dismiss();
      });
  }

  async delete() { //para solucionar el tema de list-items-sliding con ngfor
    await this.dynamicList.closeSlidingItems();
  }

  /**
   * Mostrará un spinner con el mensaje pasado por parametro
   * @param msg mensaje que mostrará
   */
  async presentLoading(msg) {
    let myloading = await this.loadingController.create({
      message: msg
    });
    return await myloading.present();
  }

  /**
   * Abrírá el modal para visualizar el templo con mas detalles
   * @param item templo que se visualizará para poder pasarlo al modal
   */
  async presentModal(item) {

    const modal = await this.ModalController.create({
      component: ModalTemploPage,
      componentProps: { item: item } //esto es para pasar datos de un lugar a otro
    });
    modal.onDidDismiss().then(
      () => {
        this.refrescar();
      });
    return await modal.present();

  }

  /**
   * Metodo para el infinite Scroll (sin implementar 😔)
   * @param event 
   */
  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      this.toggleInfiniteScroll();
      if (this.listadoPanel.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  /**
   * Cierra el infinite scroll al cargarse todos los item
   */
  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

}
