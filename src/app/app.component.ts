import { ModalInsertarImagenComponent } from './modals/modal-insertar-imagen/modal-insertar-imagen.component';
import { ModalInsertarTemploComponent } from './modals/modal-insertar-templo/modal-insertar-templo.component';
import { AuthService } from './servicios/auth.service';
import { Component, ViewChild } from '@angular/core';
import { Platform, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  @ViewChild('passwd') resetContrasena: HTMLInputElement;
  @ViewChild('username') resetUsuario: HTMLInputElement;

  private formulario: FormGroup;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private AuthService: AuthService,
    private formBuilder: FormBuilder,
    private ModalController: ModalController,
    private translate: TranslateService
  ) {
    this.initializeApp();
    this.formulario = this.formBuilder.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required],
    });

    this.platform.ready().then(() => {

      /*Gestionamos el idioma del sistema: en función del lenguaje por defecto o
      el idioma del navegador si está disponible.
      */
      this.translate.addLangs(environment.currentLanguages);  //add all languages
      this.translate.setDefaultLang(environment.defaultLanguage); //use default language
      if (this.translate.getBrowserLang) {  //if browsers's language is avalaible is set up as default
        if (environment.currentLanguages.includes(this.translate.getBrowserLang())) {
          this.translate.use(this.translate.getBrowserLang());
        }

      }
    });
  }

  /**
   * Cambia el leguaje de la aplicación
   */
  changeLang(e) {
    //console.log(e.detail.checked);
    if (e.detail.checked) {
      this.AuthService.setLang("en");
      this.translate.use("en");
    } else {
      this.AuthService.setLang("es");
      this.translate.use("es");
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  /**
   * Cierra la sesión del usuario
   */
  cerrarSesion() {
    this.AuthService.cerrarSesion();
    this.formulario.reset();
  }

  /**
   * Comprueba si los datos del formulario corresponden con algún usuario
   */
  authSesion() {

    this.AuthService.autenticacion(this.formulario.get('usuario').value, this.formulario.get('password').value);

  }

  /**
   * Limpia el campo contraseña del formulario
   */
  limpiaFormulario() {
    if (this.formulario.valid) {

      this.resetContrasena.value = "";
    }
  }

  /**
   * Comprueba si hay algún usuario logeado o no
   */
  estadoAuth() {
    return this.AuthService.estadoAuth();
  }

  /**
   * abre el modal de insertar templo
   */
  async presentModalTemplo() {

    const modal = await this.ModalController.create({
      component: ModalInsertarTemploComponent
    });

    return await modal.present();
  }

  /**
   * Abre el modal de insertar imagen
   */
  async presentModalImagen() {

    const modal = await this.ModalController.create({
      component: ModalInsertarImagenComponent
    });

    return await modal.present();
  }

}
