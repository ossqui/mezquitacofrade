import { ModalImagenPage } from './../modals/modal-imagen/modal-imagen.page';
import { Component, ViewChild } from '@angular/core';
import { ImagenesService } from '../servicios/imagenes.service';
import { LoadingController, ModalController, IonInfiniteScroll } from '@ionic/angular';
import { ConectarService } from '../servicios/conectar.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  @ViewChild('dynamicList') dynamicList;
  listado = [];
  listadoPanel = [];
  contadorImagenes = 0;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(private todoS: ImagenesService,
    public loadingController: LoadingController,
    private ModalController: ModalController,
    private comunicacion: ConectarService,
    private translate: TranslateService
  ) {
    this.presentLoading(this.translate.instant("loading"));
    this.comunicacion.getMessage().subscribe((dato) => {
      if (dato == true) {
        this.refrescar();
      }

    });
  }

  /**
   * Recarga la lista de imagenes de la base de datos al ejecutarse
   */
  refrescar() {
    this.presentLoading(this.translate.instant("loading"));
    this.todoS.leerImagenes()
      .subscribe((querySnapshot) => {
        this.listado = [];
        this.delete();
        querySnapshot.forEach((doc) => {
          // doc.data() is never undefined for query doc snapshots
          //console.log(doc.id, " => ", doc.data());
          this.listado.push({ id: doc.id, ...doc.data() });
        });
        //console.log(this.listado);
        this.listadoPanel = this.listado;
        this.loadingController.dismiss();
      });
  }

  /**
   * Detiene el spinner de cargando cuando las imagenes estan completamente cargadas
   * @param e Envento opcional que recibe por parametro (no le he dado uso)
   */
  imagenCargada(e) {

    if (++this.contadorImagenes == 2) {
      this.loadingController.dismiss();
    }

  }


  /**
   * Inicializa listadoPanel con las imagenes de la base de datos al entrar por primera vez
   */
  ionViewDidEnter() {
    this.todoS.leerImagenes()
      .subscribe((querySnapshot) => {
        this.listado = [];
        this.delete();
        querySnapshot.forEach((doc) => {
          this.listado.push({ id: doc.id, ...doc.data() });
        });
        this.listadoPanel = this.listado;
      });
  }

  /**
   * Recarga la lista de imagenes de la base de datos al ejecutarse
   */
  doRefresh(refresher) {
    this.todoS.leerImagenes()
      .subscribe(querySnapshot => {
        this.listado = [];
        this.delete(); /* Es un hack para solucionar un bug con el refresher y las listas
    dinámicas (ngFor) */
        querySnapshot.forEach((doc) => {
          this.listado.push({ id: doc.id, ...doc.data() });
        });
        this.listadoPanel = this.listado;
        refresher.target.complete();
      });
  }

  async delete() { //para solucionar el tema de list-items-sliding con ngfor
    await this.dynamicList.closeSlidingItems();
  }

  /**
   * Mostrará un spinner con el mensaje pasado por parametro
   * @param msg mensaje que mostrará
   */
  async presentLoading(msg) {
    let myloading = await this.loadingController.create({
      message: msg
    });
    return await myloading.present();
  }

  /**
   * Abrírá el modal para visualizar el templo con mas detalles
   * @param item templo que se visualizará para poder pasarlo al modal
   */
  async presentModal(item) {

    const modal = await this.ModalController.create({
      component: ModalImagenPage,
      componentProps: { item: item } //esto es para pasar datos de un lugar a otro
    });
    modal.onDidDismiss().then(
      () => {
        this.refrescar();
      });

    return await modal.present();
  }

  /**
   * Metodo para el infinite Scroll (sin implementar 😔)
   * @param event 
   */
  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      this.toggleInfiniteScroll();
      if (this.listadoPanel.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  /**
   * Cierra el infinite scroll al cargarse todos los item
   */
  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
}
