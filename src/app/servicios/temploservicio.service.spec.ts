import { TestBed } from '@angular/core/testing';

import { TemploservicioService } from './temploservicio.service';

describe('TemploservicioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TemploservicioService = TestBed.get(TemploservicioService);
    expect(service).toBeTruthy();
  });
});
