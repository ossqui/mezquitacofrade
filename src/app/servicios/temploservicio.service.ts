import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TemploservicioService {

  coleccionTemplos: AngularFirestoreCollection<any>

  constructor(private fireStore: AngularFirestore) {

    //Recupera la lista de templos de la base de datos
    this.coleccionTemplos = fireStore.collection<any>(environment.firebaseConfig.templosColeccion);

  }

  /**
   * Devuelve una coleeción con todos los templos leidos de la base de datos
   */
  leerTemplos() {
    return this.coleccionTemplos.get();
  }

  /**
   * Devuelve un templo especifico de la base de datos
   * @param id Identificador del templo a devolver
   */
  leeTemplo(id) {
    return this.coleccionTemplos.doc(id).get();
  }

  /**
   * Guarda el objeto templo pasado por parametro en la base de datos
   * @param datos Objeto templo a guardar en la base de datos
   */
  agregaTemplo(datos) {
    return this.coleccionTemplos.add(datos);
  }

  /**
   * Elimina el objeto templo deseado
   * @param id Identificador del objeto templo pasado por parametro
   */
  borraTemplo(id) {
    return this.coleccionTemplos.doc(id).delete();
  }

}
