import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { environment } from '../../environments/environment';
import { LoadingController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  usuarios: AngularFirestoreCollection<any>;
  users = [];
  listado = [];

  nombreUsuario:  String;

  autenticado = false;
  lang: any;

  constructor(private fireStore: AngularFirestore,
    public loadingController: LoadingController,
    public toastController: ToastController,
    private translate: TranslateService) {
    this.usuarios = fireStore.collection<any>(environment.firebaseConfig.usuarios);
    this.lang = environment.defaultLanguage;
  }

  /**
   * Cambia el lenguaje de la app
   * @param val Es el lenguaje al que se va a cambiar
   */
  setLang(val) {
    this.lang = val;
  }

  /**
   * Devuelve el lenguaje en el que esta la app
   */
  getLang() {
    return this.lang;
  }

  /**
   * Devuelve el estado de la autenticación
   */
  estadoAuth() {
    return this.autenticado;
  }

  /**
   * Recibe los datos del usuario y los comprueba devolviendo true si son correcto o false si no existen.
   * @param user Usuario con el que se iniciará sesión
   * @param pass Contraseña de inicio de sesión
   */
  autenticacion(user, pass) {
    this.validad(user, pass);
    return this.autenticado;
  }

  /**
   * Cierra la sesión del usuario activo
   */
  cerrarSesion() {
    this.alertaCierreSesion();
    this.autenticado = false;
  }

  /**
   * Metodo privado el cual comprobará las credenciales con la base de datos
   * @param user usuario enviado para el acceso
   * @param pass contraseña enviada para el acceso
   */
  private validad(user, pass) {
    this.presentLoading(this.translate.instant("loading"));
  
    this.usuarios.ref.where('user','==',user).where('password','==',pass).get().then(usuario => {
      if(usuario.docs.length == 0){
        this.loadingController.dismiss();
        this.alertaUsuarioIncorrecto();
        
      }else{
        this.autenticado = true;
        this.loadingController.dismiss();
        this.nombreUsuario = usuario.docs[0].data().nombre;
        this.alertaUsuarioCorrecto(this.nombreUsuario);
        
      }
    })

  }

  /**
   * Alerta que se mostrará cuando el usuario cierre la sesión
   */
  async alertaCierreSesion() {
    const toast = await this.toastController.create({
      message: this.translate.instant("closeLogin"),
      duration: 2000
    });

    await toast.present();
  }

  /**
   * Alerta que se mostrará cuando los datos del usuario no sean validos.
   */
  async alertaUsuarioIncorrecto() {
    const toast = await this.toastController.create({
      message: this.translate.instant("invalidLogin"),
      duration: 2000
    });

    await toast.present();
  }

  /**
   * Alerta que se mostraracuando el usuario sea valido y se inicie la sesión
   * @param user Nombre del usuario que mostrará la alerta
   */
  async alertaUsuarioCorrecto(user) {
    const toast = await this.toastController.create({
      message: this.translate.instant("okLogin") + user,
      duration: 2000
    });

    await toast.present();
  }

  /**
   * Mostrará un spinner con un mensaje
   * @param msg mensaje que se mostrará con el spinner
   */
  async presentLoading(msg) {
    let myloading = await this.loadingController.create({
      message: msg
    });
    return await myloading.present();
  }
}


