import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImagenesService {

  /**
   * Colección de los item imagen
   */
  coleccionImagenes: AngularFirestoreCollection<any>

  constructor(private fireStore: AngularFirestore) {

    //Recupera la lista de imagenes de la base de datos
    this.coleccionImagenes = fireStore.collection<any>(environment.firebaseConfig.imagenesColeccion);
   }

   /**
    * Devuelve la colección de las imagenes recuperadas de la base de datos
    */
   leerImagenes() {
    return this.coleccionImagenes.get();
  }

  /**
   * Devuelve una imagen especifica de la base de datos
   * @param id Identificador de la imagen a devolver
   */
  leeImagen(id) {
    return this.coleccionImagenes.doc(id).get();
  }

  /**
   * Guarda el objeto imagen pasado por parametro en la base de datos
   * @param datos Objeto imagen a guardar en la base de datos
   */
  agregaImagen(datos) {
    return this.coleccionImagenes.add(datos);
  }


  /**
   * Elimina el objeto imagen deseado
   * @param id Identificador del objeto imagen pasado por parametro
   */
  borraImagen(id) {
    return this.coleccionImagenes.doc(id).delete();
  }
}
