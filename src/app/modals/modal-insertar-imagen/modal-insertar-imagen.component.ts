import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImagenesService } from 'src/app/servicios/imagenes.service';
import { ConectarService } from 'src/app/servicios/conectar.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-modal-insertar-imagen',
  templateUrl: './modal-insertar-imagen.component.html',
  styleUrls: ['./modal-insertar-imagen.component.scss']
})
export class ModalInsertarImagenComponent implements OnInit {

  imagenSolo: string = null;
  imagenPaso: string = null;
  imagenPaso2: string = null;

  myloading: any;
  guardado: boolean = true;

  private formularioImagen: FormGroup;

  constructor(private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private imagen: ImagenesService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    private comunicacion: ConectarService,
    private translate: TranslateService) {

    this.formularioImagen = this.formBuilder.group({
      nombre: ['', Validators.required],
      descripcion: ['', Validators.required],
      descripcion2: ['', Validators.required],
      autor: ['', Validators.required],
      hermandad: ['', Validators.required],
      salida: ['', Validators.required],
      sedeCanonica: ['', Validators.required],
    });

  }

  /**
   * Guarda la foto principal de la imagen 
   * @param camara si es true la foto sera de la galería si es false sera una fotografía de la cámara
   */
  guardarImagenSolo(camara?) {

    if (camara) {
      this.galeria().then((imageData) => {
        this.imagenSolo = 'data:image/jpeg;base64,' + imageData;
        
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageGallery"));
      });
    } else {
      this.fotografia().then((imageData) => {
        this.imagenSolo = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageCamera"));
      });
    }

  }

  /**
   *  Fotografía 1 de la imagen
   * @param camara si es true la foto sera de la galería si es false sera una fotografía de la cámara
   */
  guardarImagenPaso(camara?) {
    if (camara) {
      this.galeria().then((imageData) => {
        this.imagenPaso = 'data:image/jpeg;base64,' + imageData; 
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageGallery"));
      });
    } else {
      this.fotografia().then((imageData) => {
        this.imagenPaso = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageCamera"));
      });
    }
  }

  /**
   *  Fotografía 2 de la imagen
   * @param camara si es true la foto sera de la galería si es false sera una fotografía de la cámara
   */
  guardarImagenPaso2(camara?) {
    if (camara) {
      this.galeria().then((imageData) => {
        this.imagenPaso2 = 'data:image/jpeg;base64,' + imageData; 
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageGallery"));
      });
    } else {
      this.fotografia().then((imageData) => {
        this.imagenPaso2 = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageCamera"));
      });
    }
  }

  ngOnInit() {
  }

  /**
   * Guarda los datos de la imagen añadidos si estos son validos, vacia el formulario y cierra el modal.
   */
  guardar() {

    if (this.imagenSolo != null && this.imagenPaso != null && this.imagenPaso2 != null) {
      let imagen = {
        nombre: this.formularioImagen.get('nombre').value,
        autor: this.formularioImagen.get('autor').value,
        hermandad: this.formularioImagen.get('hermandad').value,
        sedeCanonica: this.formularioImagen.get('sedeCanonica').value,
        salida: this.formularioImagen.get('salida').value,
        descripcion: this.formularioImagen.get('descripcion').value,
        descripcion2: this.formularioImagen.get('descripcion2').value,
        imagenSolo: this.imagenSolo,
        imagenPaso: this.imagenPaso,
        imagenPaso2: this.imagenPaso2
      }

      this.presentLoading();
      this.imagen.agregaImagen(imagen).then(() => {
        this.formularioImagen.setValue({
          nombre: '',
          autor: '',
          hermandad: '',
          sedeCanonica: '',
          salida: '',
          descripcion: '',
          descripcion2: ''
        });
        this.imagenPaso = null;
        this.imagenPaso2 = null;
        this.imagenSolo = null;
        this.loadingController.dismiss();
        this.notificacion(this.translate.instant("savedCorrectly"));
        this.comunicacion.sendMessage(this.guardado);
        this.closeModal();
      }).catch((error) => {
        this.loadingController.dismiss();
        console.log(error);

        this.notificacion(this.translate.instant("savedError"));
      });
    } else {
      this.notificacion(this.translate.instant("errorImages"));
    }
  }

  /**
   * Abre la galería y devuelve una promesa con la imagen que seleccionemos
   */
  galeria():Promise<string> {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 200,
      saveToPhotoAlbum: false
    }

    return this.camera.getPicture(options);
  }

  /**
   * Abre la cámara y devuelve una promesa con la imagen que capturemos
   */
  fotografia() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      targetWidth: 200,
      saveToPhotoAlbum: false
    }

    return this.camera.getPicture(options);
  }

  /**
   * muestra una notificación con un mensaje
   * @param texto mensaje que mostrará
   */
  async notificacion(texto) {
    const toast = await this.toastController.create({
      message: texto,
      duration: 2000
    });
    toast.present();
  }

  /**
   * Cierra el modal
   * @param changes evento para cerrar el modal
   */
  closeModal(changes?) {
    this.modalCtrl.dismiss(changes);
  }

  /**
   * Muestra el spinner guardado
   */
  async presentLoading() {
    this.myloading = await this.loadingController.create({
      message: this.translate.instant("saveItem")
    });
    return await this.myloading.present();
  }

}
