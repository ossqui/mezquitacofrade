import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertarImagenComponent } from './modal-insertar-imagen.component';

describe('ModalInsertarImagenComponent', () => {
  let component: ModalInsertarImagenComponent;
  let fixture: ComponentFixture<ModalInsertarImagenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertarImagenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertarImagenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
