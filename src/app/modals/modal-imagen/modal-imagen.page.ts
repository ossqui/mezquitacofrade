import { Component, OnInit, Input } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { ImagenesService } from 'src/app/servicios/imagenes.service';
import { AuthService } from 'src/app/servicios/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-modal-imagen',
  templateUrl: './modal-imagen.page.html',
  styleUrls: ['./modal-imagen.page.scss'],
})
export class ModalImagenPage implements OnInit {

  @Input() item: Object;

  constructor(private modalCtrl: ModalController,
  private servicioImagen: ImagenesService,
  private AuthService: AuthService,
  private translate: TranslateService,
  public loadingController: LoadingController,
    ) { 
      this.presentLoading(this.translate.instant("loading"));
    }

  ngOnInit() {
  }

  /**
   * cierra el spinner de cargando al cargarse la imagen principal
   */
  imagenCargada() {
    this.loadingController.dismiss();
  }

  /**
   * Borra la imagen que esta en el modal y lo cierra
   * @param id Identificador del la imagen
   * @param changes Evento opcional que se pasa al cerrar el modal
   */
  borrarImagen(id,changes?){
    this.servicioImagen.borraImagen(id);
    this.modalCtrl.dismiss(changes);
  }

  /**
   * Cierra el modal
   * @param changes evento que se le pasará a cerrar el modal
   */
  closeModal(changes?) {
    this.modalCtrl.dismiss(changes);
  }

  /**
   * Comprueba que esta autenticado el usuario
   */
  estadoAuth() {
    return this.AuthService.estadoAuth();
  }

  /**
   * Muestra un spinner con un mensaje
   * @param msg mensaje que mostrará
   */
  async presentLoading(msg) {
    let myloading = await this.loadingController.create({
      message: msg
    });
    return await myloading.present();
  }
}
