import { AuthService } from './../../servicios/auth.service';
import { Component, OnInit, Input } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { TemploservicioService } from 'src/app/servicios/temploservicio.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-modal-templo',
  templateUrl: './modal-templo.page.html',
  styleUrls: ['./modal-templo.page.scss'],
})
export class ModalTemploPage implements OnInit {

  @Input() item: Object;


  cargado: boolean = false;

  /**
   * Opciones del slide
   */
  slideOpts = {
    effect: 'flip'
  };

  constructor(
    private modalCtrl: ModalController,
    public loadingController: LoadingController,
    private servicioTemplo: TemploservicioService,
    private AuthService: AuthService,
    private translate: TranslateService
  ) {
    this.presentLoading(this.translate.instant("loading"));
  }

  /**
   * cierra el spinner de cargando al cargarse la imagen principal
   */
  imagenCargada() {
    this.loadingController.dismiss();
  }

  ngOnInit() {
  }

  /**
   * Borra el templo que esta en el modal y lo cierra
   * @param id Identificador del templo
   * @param changes Evento opcional que se pasa al cerrar el modal
   */
  borrarTemplo(id, changes?) {
    this.servicioTemplo.borraTemplo(id);
    this.modalCtrl.dismiss(changes);
  }

  /**
   * Cierra el modal
   * @param changes evento que se le pasará a cerrar el modal
   */
  closeModal(changes?) {
    this.modalCtrl.dismiss(changes);
  }

  

  /**
   * Muestra un spinner con un mensaje
   * @param msg mensaje que mostrará
   */
  async presentLoading(msg) {
    let myloading = await this.loadingController.create({
      message: msg
    });
    return await myloading.present();
  }

  /**
   * Comprueba si hay algun usuario autenticado
   */
  estadoAuth() {
    return this.AuthService.estadoAuth();
  }

}
