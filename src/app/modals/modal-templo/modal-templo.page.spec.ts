import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTemploPage } from './modal-templo.page';

describe('ModalTemploPage', () => {
  let component: ModalTemploPage;
  let fixture: ComponentFixture<ModalTemploPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTemploPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTemploPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
