import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertarTemploComponent } from './modal-insertar-templo.component';

describe('ModalInsertarTemploComponent', () => {
  let component: ModalInsertarTemploComponent;
  let fixture: ComponentFixture<ModalInsertarTemploComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertarTemploComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertarTemploComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
