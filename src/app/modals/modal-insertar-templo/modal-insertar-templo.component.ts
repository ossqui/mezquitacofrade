import { TemploservicioService } from './../../servicios/temploservicio.service';
import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ConectarService } from 'src/app/servicios/conectar.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-modal-insertar-templo',
  templateUrl: './modal-insertar-templo.component.html',
  styleUrls: ['./modal-insertar-templo.component.scss']
})
export class ModalInsertarTemploComponent implements OnInit {

  image1: string = null;
  image2: string = null;
  image3: string = null;
  image4: string = null;

  myloading: any;
  guardado: boolean = true;

  private formularioTemplo: FormGroup;

  constructor(private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private templo: TemploservicioService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    private comunicacion: ConectarService,
    private translate: TranslateService
  ) {

    this.formularioTemplo = this.formBuilder.group({
      nombre: ['', Validators.required],
      descripcion: ['', Validators.required],
      horaAM: ['', Validators.required],
      horaCM: ['', Validators.required],
      horaAT: ['', Validators.required],
      horaCT: ['', Validators.required],
    });


  }

  /**
   * Guarda la foto principal del templo
   * @param camara si es true la foto sera de la galería si es false sera una fotografía de la cámara
   */
  guardarImage1(camara?){
    if (camara) {
      this.galeria().then((imageData) => {
        this.image1 = 'data:image/jpeg;base64,' + imageData;
        
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageGallery"));
      });
    } else {
      this.fotografia().then((imageData) => {
        this.image1 = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageCamera"));
      });
    }
  }

  /**
   *  Fotografía galería 1 del templo
   * @param camara si es true la foto sera de la galería si es false sera una fotografía de la cámara
   */
  guardarImage2(camara?){
    if (camara) {
      this.galeria().then((imageData) => {
        this.image2 = 'data:image/jpeg;base64,' + imageData;
        
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageGallery"));
      });
    } else {
      this.fotografia().then((imageData) => {
        this.image2 = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageCamera"));
      });
    }
  }

  /**
   *  Fotografía galería 2 del templo
   * @param camara si es true la foto sera de la galería si es false sera una fotografía de la cámara
   */
  guardarImage3(camara?){
    if (camara) {
      this.galeria().then((imageData) => {
        this.image3 = 'data:image/jpeg;base64,' + imageData;
        
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageGallery"));
      });
    } else {
      this.fotografia().then((imageData) => {
        this.image3 = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageCamera"));
      });
    }
  }

  /**
   *  Fotografía galería 3 del templo
   * @param camara si es true la foto sera de la galería si es false sera una fotografía de la cámara
   */
  guardarImage4(camara?){
    if (camara) {
      this.galeria().then((imageData) => {
        this.image4 = 'data:image/jpeg;base64,' + imageData;
        
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageGallery"));
      });
    } else {
      this.fotografia().then((imageData) => {
        this.image4 = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
        this.notificacion(this.translate.instant("errorImageCamera"));
      });
    }
  }

  /**
   * muestra una notificación con un mensaje
   * @param texto mensaje que mostrará
   */
  async notificacion(texto) {
    const toast = await this.toastController.create({
      message: texto,
      duration: 2000
    });
    toast.present();
  }


  /**
   * Guarda los datos de la imagen añadidos si estos son validos, vacia el formulario y cierra el modal.
   */
  guardar() {
    
    if (this.image1 != null) {
      let templo = {
        nombre: this.formularioTemplo.get('nombre').value,
        horaAperturaManana: this.formularioTemplo.get('horaAM').value,
        horaAperturaTarde: this.formularioTemplo.get('horaCM').value,
        horaCierreManana: this.formularioTemplo.get('horaAT').value,
        horaCierreTarde: this.formularioTemplo.get('horaCT').value,
        descripcion: this.formularioTemplo.get('descripcion').value,
        imagen1: this.image1,
        imagen2: this.image2,
        imagen3: this.image3,
        imagen4: this.image4
      }

      this.presentLoading();
      this.templo.agregaTemplo(templo).then(() => {
        this.formularioTemplo.setValue({
          nombre: '',
          descripcion: '',
          horaAM: '',
          horaCM: '',
          horaAT: '',
          horaCT: ''
        });
        this.image1 = null;
        this.image2 = null;
        this.image3 = null;
        this.image4 = null;
        this.loadingController.dismiss();
        this.notificacion(this.translate.instant("savedCorrectly"));
        
        this.comunicacion.sendMessage(this.guardado);
        this.closeModal();
      }).catch((error) => {
        this.loadingController.dismiss();
        console.log(error);
        
        this.notificacion(this.translate.instant("savedError"));
      });
      
    }else{
      this.notificacion(this.translate.instant("errorMainImage"));
    }
  }


  /**
   * Abre la galería y devuelve una promesa con la imagen que seleccionemos
   */
  galeria():Promise<string> {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 200,
      saveToPhotoAlbum: false
    }

    return this.camera.getPicture(options);
  }


  /**
   * Abre la cámara y devuelve una promesa con la imagen que capturemos
   */
  fotografia() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      targetWidth: 200,
      saveToPhotoAlbum: false
    }

    return this.camera.getPicture(options);
  }

  ngOnInit() {
  }

  /**
   * Cierra el modal
   * @param changes evento para cerrar el modal
   */
  closeModal(changes?) {
    this.modalCtrl.dismiss(changes);
  }

  /**
   * Muestra el spinner guardado
   */
  async presentLoading() {
    this.myloading = await this.loadingController.create({
      message: this.translate.instant("saveItem")
    });
    return await this.myloading.present();
  }
}
